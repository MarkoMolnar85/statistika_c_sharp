﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;        //neophodan za file-ove i slicno i string reader
namespace VladimirMirkovic
{
    public partial class Form1 : Form
    {
        Statistika stat = null;
        bool loadDa = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            loadDa = true;
            double[] UlazniPodaci = null;
            int brojRedova = 0;
            int i = 0;        //brojac donja petlja
            TextReader citac = new StringReader(txtUnosPodataka.Text);
            for (string line = citac.ReadLine(); line != null; line = citac.ReadLine())  //cita texkt po redovima
            {
                brojRedova++;
            }
            UlazniPodaci = new double[brojRedova];
            citac = null;
            citac = new StringReader(txtUnosPodataka.Text);
            for (string line = citac.ReadLine(); line != null; line = citac.ReadLine())
            {
                UlazniPodaci[i] = Convert.ToDouble(line);
                i++;
            }
            stat = new Statistika(UlazniPodaci, brojRedova);
        }

        private void btnFreqRazredi_Click(object sender, EventArgs e)
        {
            if(loadDa)
            lblIntervaliFreqRazredi.Text = stat.IspisIntervalaFreqRazredi();
        }

        private void btnIntervaliFreq_Click(object sender, EventArgs e)
        {
            if(loadDa)
            lblIntervaliFreq.Text = stat.IspisiVrednostiFrekfencije();
        }

        private void btnDeskriptivnaStatistika_Click(object sender, EventArgs e)
        {
            if(loadDa)
            lblDeskriptivnaStatistika.Text = stat.DeskriptivnaStatistika();
        }

        private void btnCrtaj_Click(object sender, EventArgs e)
        {
            if (loadDa)
            {
                Freq[] f = stat.unosFrekfencija();
                for (int i = 0; i < f.Length; i++)
                {
                    this.chartStatistika.Series["frekfencije"].Points.AddXY(f[i].vrednost.ToString(), f[i].frekfencijaZaVrendost.ToString());
                }
            }
            if (loadDa)
            {
                IntervaliRazredi[] ir = stat.Razredi();
                for (int i = 0; i < ir.Length; i++)
                {
                    string text = ir[i].donjiRazred.ToString() + " - "+ ir[i].gornjiRazred.ToString();
                    this.cartRazredi.Series["razrediPita"].Points.AddXY(text, ir[i].freq.ToString());
                }
            }
        }
    }
}
