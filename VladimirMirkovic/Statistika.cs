﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VladimirMirkovic
{
    class Statistika
    {
        private double [] nizPodataka;    //privatni niz svi podaci
        private double max;
        private double min;
        private double sum;
        private double xBar;     //aritmeticka sredina
        private int count;
        private double disperzija;
        private double disperzijaKorigovana;
        private double stdev;
        private double standardnaGreska;
        //private double koeficijentAsimetrije;
        //private double koeficijentSpljostenosti;
        public Statistika(double[] nizArg, int n)   //konstruktor
        {
            nizPodataka = new double[n];          //inicijalizacija niza
            for (int i = 0; i < n; i++)
            {
                nizPodataka[i] = nizArg[i];
            }
            for (int i = 0; i < n - 1; i++)        //sortiranje niza od manjeg do veceg
            {
                for (int j = i + 1; j < n; j++)
                {
                    double temp = 0;
                    if (nizPodataka[i] > nizPodataka[j])
                    {
                        temp = nizPodataka[i];
                        nizPodataka[i] = nizPodataka[j];
                        nizPodataka[j] = temp;
                    }
                }
            }
        }
        public string SortiraniNiz()      //u principu ne sluzi nicemu samo za proveru
        {
            string s = "" ;
            for (int i = 0; i < nizPodataka.Length; i++)
            {
                s = s + nizPodataka[i] + "\n";
            }
            return s;
        }
        private void MaximalnaVrednost()
        {
            max = nizPodataka[0];
            for (int i = 0; i < nizPodataka.Length; i++)
            {
                if (max < nizPodataka[i])
                {
                    max = nizPodataka[i];
                }
            }
        }
        private void MinimalnaVrednost()
        {
            min = nizPodataka[0];
            for (int i = 0; i < nizPodataka.Length; i++)
            {
                if (min > nizPodataka[i])
                {
                    min = nizPodataka[i];
                }
            }
        }
        private void sumaSvih()
        {
            sum = 0;
            for (int i = 0; i < nizPodataka.Length; i++)
            {
                sum += nizPodataka[i];
            }
        }
        private double sumaSvihpovratna()
        {
            double sumA = 0;
            for (int i = 0; i < nizPodataka.Length; i++)
            {
                sumA += nizPodataka[i];
            }
            return sumA;
        }
        private void aritmetickaSredina()
        {
            xBar = sumaSvihpovratna() / nizPodataka.Length;
        }
        private double aritmetickaSredinaPovratna()
        {
            return sumaSvihpovratna() / nizPodataka.Length;
        }
        
        public IntervaliRazredi[] Razredi()
        {
            int brojIntervala = 0;
            MaximalnaVrednost(); MinimalnaVrednost();
            int razlika = (int)(max - min);
            int duzinarazreada = (int)Math.Sqrt(razlika) +1;
            brojIntervala = duzinarazreada;

            IntervaliRazredi []ir = new IntervaliRazredi[brojIntervala];
            ir[0] = new IntervaliRazredi();        //kreiranje objekta za [0]
            ir[0].donjiRazred = min;
            ir[0].gornjiRazred = min + duzinarazreada;
            ir[0].xsi = (ir[0].donjiRazred - ir[0].gornjiRazred) /2;
            for (int i = 1; i < brojIntervala; i++)
            {
                ir[i] = new IntervaliRazredi();
                ir[i].donjiRazred = ir[i - 1].donjiRazred + duzinarazreada;
                ir[i].gornjiRazred = ir[i - 1].gornjiRazred + duzinarazreada;
                ir[i].xsi = (ir[i].donjiRazred + ir[i].gornjiRazred) /2;                
            }
            for (int i = 0; i < ir.Length; i++)
            {
                for (int j = 0; j < nizPodataka.Length; j++)
                {
                    if (nizPodataka[j] >= ir[i].donjiRazred && nizPodataka[j] < ir[i].gornjiRazred)
                    {
                        ir[i].freq++;
                    }
                }
            }
            return ir;
        }       //kraj metode razredi
        public string IspisIntervalaFreqRazredi()
        {
            IntervaliRazredi[] ifr = Razredi();
            string ispis = "";
            foreach (IntervaliRazredi i in ifr)
            {
                //ispis += i.donjiRazred.ToString() + " <= " + " Xsi " + i.xsi.ToString() + " < " + i.gornjiRazred.ToString() + " Freq " + i.freq.ToString() + "\n"; 
                ispis += "OD  " + i.donjiRazred + " DO " + i.gornjiRazred + " Freq = " + i.freq + "\n";
            }
            return ispis;
        }
        public Freq[] unosFrekfencija()
        {
            List<double> listaRazlicitih = new List<double>();
            foreach (double d in nizPodataka)
            {
                listaRazlicitih.Add(d);
            }
            int brojacPonavljanja = 0;
            for(int i = 0;  i < nizPodataka.Length; i++)
            {
                for(int j = i+1;  j < nizPodataka.Length; j++)
                {
                    if(listaRazlicitih[i] == listaRazlicitih[j])
                    {
                        listaRazlicitih.RemoveAt(i);
                        listaRazlicitih.Insert(i, -999);
                        brojacPonavljanja++;
                    }
                }
            }
            for(int i =0; i < brojacPonavljanja; i++)
            {
                listaRazlicitih.Remove(-999);
            }
            int n = 0;
            foreach (double z in listaRazlicitih)
            {
                n++;
            }
            //int n = listaRazlicitih.Count;
            
            Freq[]  ff = new Freq[n];
            for (int i = 0; i < ff.Length; i++)
            {
                ff[i] = new Freq();
                ff[i].vrednost = listaRazlicitih[i];
            }
            for (int i = 0; i < ff.Length; i++)
            {
                for (int j = 0; j < nizPodataka.Length; j++)
                {
                    if (ff[i].vrednost == nizPodataka[j])
                    {
                        ff[i].frekfencijaZaVrendost++;
                    }
                }
            }
            return ff;
        }//kraj metode za frekfenciju
        public string IspisiVrednostiFrekfencije()
        {
            Freq[] ff = unosFrekfencija();
            string i = "";
            foreach (Freq f in ff)
            {
                
                i = i + "Vrednost: " + f.vrednost.ToString() + " Fi: " + f.frekfencijaZaVrendost.ToString() + "\n";
            }
            return i;
        }
        private void countSvihClanova()
        {
            count = nizPodataka.Length;
        }
        private double Range()
        {
            MaximalnaVrednost(); MinimalnaVrednost();
            return max - min;
        }
        private double Modus()
        {
            Freq[] ff = unosFrekfencija();
            int max1 = 0;//ff[0].frekfencijaZaVrendost;
            int max2 = 0;//ff[0].frekfencijaZaVrendost;
            int lokacijaMax1 = 0;
            int lokacijamax2 = 0;
            for (int i = 0; i < ff.Length; i++)
            {
                if (max1 < ff[i].frekfencijaZaVrendost)
                {
                    max1 = ff[i].frekfencijaZaVrendost;
                    lokacijaMax1 = i;
                }
            }
            for (int i = 0; i < ff.Length; i++)
            {
                if (max2 < ff[i].frekfencijaZaVrendost && i != lokacijaMax1)
                {
                    max2 = ff[i].frekfencijaZaVrendost;
                    lokacijamax2 = i;
                }
            }
            double povratnaVrednost = 0;
            if (ff[lokacijaMax1].frekfencijaZaVrendost > ff[lokacijamax2].frekfencijaZaVrendost)
            {
                povratnaVrednost = ff[lokacijaMax1].vrednost;
            }
            else if (ff[lokacijaMax1].frekfencijaZaVrendost < ff[lokacijamax2].frekfencijaZaVrendost)
            {
                povratnaVrednost = ff[lokacijamax2].vrednost;
            }
            else
            {
                povratnaVrednost = -999;
            }
            return povratnaVrednost;
        }//kraj metode modus
        private double Mediana()
        {
            double[] nizOvajSkope = new double[nizPodataka.Length];
            nizPodataka.CopyTo(nizOvajSkope, 0);        //kopira levi u desni sa pocetkom nula
            double median = 0;
            if (nizOvajSkope.Length % 2 == 0)
            {
                median = (nizOvajSkope[nizOvajSkope.Length / 2 - 1] + nizOvajSkope[nizOvajSkope.Length / 2]) / 2;
            }
            else
            {
                median = nizOvajSkope[nizOvajSkope.Length / 2];
            }
            return median;
        }//kraj mediana
        private void disperzijaIzracunaj()     //racuna obe disperzije 
        {
            aritmetickaSredina();
            countSvihClanova();
            double[] nizOvajSkope = new double[nizPodataka.Length];
            nizPodataka.CopyTo(nizOvajSkope, 0);
            double xBar = this.xBar;
            double n = (double)this.count;
            double suma = 0;
            for (int i = 0; i < nizOvajSkope.Length; i++)
            {
                suma += (nizOvajSkope[i] - xBar) * (nizOvajSkope[i] - xBar);
            }
            this.disperzija = suma / n;
            this.disperzijaKorigovana = 1 / (n - 1) * suma;
        }
        private void standardnaDevijacijaIzracunaj()
        {
            this.stdev = Math.Sqrt(this.disperzijaKorigovana);
        }
        private void standardnaGreskaIracunaj()
        {           
            double n = this.nizPodataka.Length;
            this.standardnaGreska = stdev / Math.Sqrt(n);
        }
        //private void SpljostenostAsimetrijaIzracunaj()
        //{
        //    countSvihClanova();
        //    aritmetickaSredina();
        //    disperzijaIzracunaj();
        //    standardnaDevijacijaIzracunaj();
        //    double xBar = this.xBar;
        //    double M3 = 0; double M4 = 0;
        //    double sum3 = 0, sum4 = 0;
        //    double[] nizOvajSkope = new double[this.nizPodataka.Length];
        //    this.nizPodataka.CopyTo(nizOvajSkope, 0);
        //    for (int i = 0; i < nizOvajSkope.Length; i++)
        //    {
        //        sum3 += Math.Pow((nizOvajSkope[i] - xBar), 3);
        //        sum4 += Math.Pow((nizOvajSkope[i] - xBar), 4);
        //    }
        //    M3 = sum3;
        //    M4 = sum4;
        //    koeficijentAsimetrije = M3 / Math.Pow(this.disperzija, 3);
        //    koeficijentSpljostenosti = M4 / Math.Pow(this.disperzija, 4);
        //}
        public string DeskriptivnaStatistika()
        {
            MinimalnaVrednost();
            MaximalnaVrednost();
            countSvihClanova();
            disperzijaIzracunaj();
            standardnaDevijacijaIzracunaj();
            standardnaGreskaIracunaj();
            //SpljostenostAsimetrijaIzracunaj();
            string ispisDeskriptivne = "";
            ispisDeskriptivne += "Aritmeticka sredina: " + aritmetickaSredinaPovratna().ToString() + "\n";
            ispisDeskriptivne += "Minimum: " + min.ToString() + "\n";
            ispisDeskriptivne += "Maximim: " + max.ToString() + "\n";
            ispisDeskriptivne += "Sum: " + sumaSvihpovratna().ToString() + "\n";
            ispisDeskriptivne += "Count: " + count.ToString() + "\n";
            ispisDeskriptivne += "Raspon: " + Range().ToString() + "\n";
            double modusOvajSkope = Modus();
            if (modusOvajSkope == -999)
            {
                ispisDeskriptivne += "Modus: NEMA\n";
            }
            else
            {
                ispisDeskriptivne += "Modus: " + modusOvajSkope.ToString() + "\n";
            }
            ispisDeskriptivne += "Mediana: " + Mediana().ToString() + "\n";
            ispisDeskriptivne += "Disperzija: " + disperzija.ToString() + "\n";
            ispisDeskriptivne += "S korigovano: " + disperzijaKorigovana.ToString() + "\n";
            ispisDeskriptivne += "STDEV: " + this.stdev.ToString() + "\n";
            ispisDeskriptivne += "Standardna greska: " + this.standardnaGreska.ToString() + "\n";
            //ispisDeskriptivne += "Asimetrija: " + this.koeficijentAsimetrije.ToString() + "\n";
            //ispisDeskriptivne += "Spljostenost: " + this.koeficijentSpljostenosti.ToString() + "\n";
            return ispisDeskriptivne;
            

        }


    }
}
