﻿namespace VladimirMirkovic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.txtUnosPodataka = new System.Windows.Forms.TextBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.lblIntervaliFreqRazredi = new System.Windows.Forms.Label();
            this.btnFreqRazredi = new System.Windows.Forms.Button();
            this.btnIntervaliFreq = new System.Windows.Forms.Button();
            this.lblIntervaliFreq = new System.Windows.Forms.Label();
            this.btnDeskriptivnaStatistika = new System.Windows.Forms.Button();
            this.lblDeskriptivnaStatistika = new System.Windows.Forms.Label();
            this.chartStatistika = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnCrtaj = new System.Windows.Forms.Button();
            this.cartRazredi = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chartStatistika)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartRazredi)).BeginInit();
            this.SuspendLayout();
            // 
            // txtUnosPodataka
            // 
            this.txtUnosPodataka.Location = new System.Drawing.Point(12, 12);
            this.txtUnosPodataka.Multiline = true;
            this.txtUnosPodataka.Name = "txtUnosPodataka";
            this.txtUnosPodataka.Size = new System.Drawing.Size(80, 433);
            this.txtUnosPodataka.TabIndex = 0;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(12, 451);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(80, 53);
            this.btnLoad.TabIndex = 1;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // lblIntervaliFreqRazredi
            // 
            this.lblIntervaliFreqRazredi.AutoSize = true;
            this.lblIntervaliFreqRazredi.Location = new System.Drawing.Point(98, 58);
            this.lblIntervaliFreqRazredi.Name = "lblIntervaliFreqRazredi";
            this.lblIntervaliFreqRazredi.Size = new System.Drawing.Size(99, 13);
            this.lblIntervaliFreqRazredi.TabIndex = 4;
            this.lblIntervaliFreqRazredi.Text = "Intervali freq razredi";
            // 
            // btnFreqRazredi
            // 
            this.btnFreqRazredi.Location = new System.Drawing.Point(98, 12);
            this.btnFreqRazredi.Name = "btnFreqRazredi";
            this.btnFreqRazredi.Size = new System.Drawing.Size(96, 41);
            this.btnFreqRazredi.TabIndex = 5;
            this.btnFreqRazredi.Text = "Intervali i freq";
            this.btnFreqRazredi.UseVisualStyleBackColor = true;
            this.btnFreqRazredi.Click += new System.EventHandler(this.btnFreqRazredi_Click);
            // 
            // btnIntervaliFreq
            // 
            this.btnIntervaliFreq.Location = new System.Drawing.Point(285, 12);
            this.btnIntervaliFreq.Name = "btnIntervaliFreq";
            this.btnIntervaliFreq.Size = new System.Drawing.Size(104, 41);
            this.btnIntervaliFreq.TabIndex = 6;
            this.btnIntervaliFreq.Text = "Intervali freq";
            this.btnIntervaliFreq.UseVisualStyleBackColor = true;
            this.btnIntervaliFreq.Click += new System.EventHandler(this.btnIntervaliFreq_Click);
            // 
            // lblIntervaliFreq
            // 
            this.lblIntervaliFreq.AutoSize = true;
            this.lblIntervaliFreq.Location = new System.Drawing.Point(282, 56);
            this.lblIntervaliFreq.Name = "lblIntervaliFreq";
            this.lblIntervaliFreq.Size = new System.Drawing.Size(65, 13);
            this.lblIntervaliFreq.TabIndex = 7;
            this.lblIntervaliFreq.Text = "Intervali freq";
            // 
            // btnDeskriptivnaStatistika
            // 
            this.btnDeskriptivnaStatistika.Location = new System.Drawing.Point(445, 12);
            this.btnDeskriptivnaStatistika.Name = "btnDeskriptivnaStatistika";
            this.btnDeskriptivnaStatistika.Size = new System.Drawing.Size(112, 41);
            this.btnDeskriptivnaStatistika.TabIndex = 8;
            this.btnDeskriptivnaStatistika.Text = "Deskriptivna statistika";
            this.btnDeskriptivnaStatistika.UseVisualStyleBackColor = true;
            this.btnDeskriptivnaStatistika.Click += new System.EventHandler(this.btnDeskriptivnaStatistika_Click);
            // 
            // lblDeskriptivnaStatistika
            // 
            this.lblDeskriptivnaStatistika.AutoSize = true;
            this.lblDeskriptivnaStatistika.Location = new System.Drawing.Point(447, 56);
            this.lblDeskriptivnaStatistika.Name = "lblDeskriptivnaStatistika";
            this.lblDeskriptivnaStatistika.Size = new System.Drawing.Size(110, 13);
            this.lblDeskriptivnaStatistika.TabIndex = 9;
            this.lblDeskriptivnaStatistika.Text = "Deskriptivna statistika";
            // 
            // chartStatistika
            // 
            this.chartStatistika.BackColor = System.Drawing.Color.DarkKhaki;
            chartArea1.Name = "ChartArea1";
            this.chartStatistika.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartStatistika.Legends.Add(legend1);
            this.chartStatistika.Location = new System.Drawing.Point(445, 306);
            this.chartStatistika.Name = "chartStatistika";
            series1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.HorizontalCenter;
            series1.BackSecondaryColor = System.Drawing.Color.Tomato;
            series1.BorderColor = System.Drawing.Color.Lime;
            series1.ChartArea = "ChartArea1";
            series1.Color = System.Drawing.Color.Lime;
            series1.Legend = "Legend1";
            series1.Name = "frekfencije";
            series1.YValuesPerPoint = 2;
            this.chartStatistika.Series.Add(series1);
            this.chartStatistika.Size = new System.Drawing.Size(578, 198);
            this.chartStatistika.TabIndex = 10;
            this.chartStatistika.Text = "chart1";
            // 
            // btnCrtaj
            // 
            this.btnCrtaj.Location = new System.Drawing.Point(1029, 306);
            this.btnCrtaj.Name = "btnCrtaj";
            this.btnCrtaj.Size = new System.Drawing.Size(71, 198);
            this.btnCrtaj.TabIndex = 11;
            this.btnCrtaj.Text = "Prikazi";
            this.btnCrtaj.UseVisualStyleBackColor = true;
            this.btnCrtaj.Click += new System.EventHandler(this.btnCrtaj_Click);
            // 
            // cartRazredi
            // 
            chartArea2.Name = "ChartArea1";
            this.cartRazredi.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.cartRazredi.Legends.Add(legend2);
            this.cartRazredi.Location = new System.Drawing.Point(741, 17);
            this.cartRazredi.Name = "cartRazredi";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series2.Legend = "Legend1";
            series2.Name = "razrediPita";
            this.cartRazredi.Series.Add(series2);
            this.cartRazredi.Size = new System.Drawing.Size(358, 273);
            this.cartRazredi.TabIndex = 12;
            this.cartRazredi.Text = "chart1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 516);
            this.Controls.Add(this.cartRazredi);
            this.Controls.Add(this.btnCrtaj);
            this.Controls.Add(this.chartStatistika);
            this.Controls.Add(this.lblDeskriptivnaStatistika);
            this.Controls.Add(this.btnDeskriptivnaStatistika);
            this.Controls.Add(this.lblIntervaliFreq);
            this.Controls.Add(this.btnIntervaliFreq);
            this.Controls.Add(this.btnFreqRazredi);
            this.Controls.Add(this.lblIntervaliFreqRazredi);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.txtUnosPodataka);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.chartStatistika)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cartRazredi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtUnosPodataka;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label lblIntervaliFreqRazredi;
        private System.Windows.Forms.Button btnFreqRazredi;
        private System.Windows.Forms.Button btnIntervaliFreq;
        private System.Windows.Forms.Label lblIntervaliFreq;
        private System.Windows.Forms.Button btnDeskriptivnaStatistika;
        private System.Windows.Forms.Label lblDeskriptivnaStatistika;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartStatistika;
        private System.Windows.Forms.Button btnCrtaj;
        private System.Windows.Forms.DataVisualization.Charting.Chart cartRazredi;
    }
}

