﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VladimirMirkovic
{
    class IntervaliRazredi
    {
        public double donjiRazred { get; set; }
        public double gornjiRazred { get; set; }
        public double xsi { get; set; }
        public int freq { get; set; }

    }
}
